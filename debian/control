Source: pg-cron
Section: database
Priority: optional
Maintainer: Debian PostgreSQL Maintainers <team+postgresql@tracker.debian.org>
Uploaders:
 Christoph Berg <myon@debian.org>,
Build-Depends:
 architecture-is-64-bit <!pkg.postgresql.32-bit>,
 debhelper-compat (= 13),
 postgresql-all <!nocheck>,
 postgresql-server-dev-all,
Standards-Version: 4.7.0
Rules-Requires-Root: no
Vcs-Browser: https://salsa.debian.org/postgresql/pg-cron
Vcs-Git: https://salsa.debian.org/postgresql/pg-cron.git
Homepage: https://github.com/citusdata/pg_cron

Package: postgresql-17-cron
Architecture: any
Depends:
 ${misc:Depends},
 ${postgresql:Depends},
 ${shlibs:Depends},
Description: Run periodic jobs in PostgreSQL
 pg_cron is a simple cron-based job scheduler for PostgreSQL (9.5 or higher)
 that runs inside the database as an extension. It uses the same syntax as
 regular cron, but it allows you to schedule PostgreSQL commands directly from
 the database. pg_cron can run multiple jobs in parallel, but it runs at most
 one instance of a job at a time. If a second run is supposed to start before
 the first one finishes, then the second run is queued and started as soon as
 the first run completes.
