pg-cron (1.6.5-1) unstable; urgency=medium

  * New upstream version 1.6.5.

 -- Christoph Berg <myon@debian.org>  Tue, 17 Dec 2024 19:44:16 +0100

pg-cron (1.6.4-2) unstable; urgency=medium

  * Upload for PostgreSQL 17.
  * Restrict to 64-bit architectures.
  * Mark postgresql-all as <!nocheck>.

 -- Christoph Berg <myon@debian.org>  Fri, 13 Sep 2024 18:11:33 +0200

pg-cron (1.6.4-1) unstable; urgency=medium

  * New upstream version 1.6.4.

 -- Christoph Berg <myon@debian.org>  Sat, 17 Aug 2024 17:16:46 +0200

pg-cron (1.6.3-1) unstable; urgency=medium

  * New upstream version 1.6.3.

 -- Christoph Berg <myon@debian.org>  Wed, 07 Aug 2024 17:06:04 +0200

pg-cron (1.6.2-1) unstable; urgency=medium

  * New upstream version 1.6.2.

 -- Christoph Berg <myon@debian.org>  Mon, 23 Oct 2023 13:35:21 +0200

pg-cron (1.6.1-1) unstable; urgency=medium

  * New upstream version 1.6.1.

 -- Christoph Berg <myon@debian.org>  Tue, 17 Oct 2023 16:41:10 +0200

pg-cron (1.6.0-1) unstable; urgency=medium

  * New upstream version 1.6.0.
  * Upload for PostgreSQL 16.
  * Use ${postgresql:Depends}.

 -- Christoph Berg <myon@debian.org>  Sun, 17 Sep 2023 18:58:47 +0200

pg-cron (1.5.2-2) unstable; urgency=medium

  * Fix compatibility with PG16.
  * Remove K&R function definitions, clang-15 doesn't like them.

 -- Christoph Berg <myon@debian.org>  Mon, 31 Jul 2023 20:07:53 +0200

pg-cron (1.5.2-1) experimental; urgency=medium

  * New upstream version 1.5.2.
  * Test latest version instead of hardcoded value 1.4.

 -- Christoph Berg <myon@debian.org>  Fri, 12 May 2023 18:48:11 +0200

pg-cron (1.5.1-1) experimental; urgency=medium

  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Mon, 13 Mar 2023 12:13:35 +0100

pg-cron (1.4.2-2) unstable; urgency=medium

  * Upload for PostgreSQL 15.
  * debian/watch: Look at GitHub tags instead of releases.

 -- Christoph Berg <myon@debian.org>  Tue, 01 Nov 2022 16:03:09 +0100

pg-cron (1.4.2-1) unstable; urgency=medium

  * New upstream version.
  * Makefile: Disable temp instance for regression tests.

 -- Christoph Berg <myon@debian.org>  Mon, 08 Aug 2022 19:52:11 +0200

pg-cron (1.4.1-2) unstable; urgency=medium

  * Fix testsuite failure on some 32-bit architectures.

 -- Christoph Berg <myon@debian.org>  Wed, 03 Nov 2021 13:30:57 +0100

pg-cron (1.4.1-1) unstable; urgency=medium

  * Upload for PG14.

 -- Christoph Berg <myon@debian.org>  Fri, 15 Oct 2021 14:22:13 +0200

pg-cron (1.3.1-1) experimental; urgency=medium

  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Thu, 22 Apr 2021 17:15:31 +0200

pg-cron (1.3.0-2) unstable; urgency=medium

  * R³: no.
  * Work around problem in PG13.0's atomics/arch-ppc.h.

 -- Christoph Berg <myon@debian.org>  Fri, 09 Oct 2020 13:22:42 +0200

pg-cron (1.3.0-1) unstable; urgency=low

  [ Debian Janitor ]
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Set upstream metadata fields: Bug-Database, Bug-Submit, Repository,
    Repository-Browse.

  [ Christoph Berg ]
  * New upstream version with PG13 support.
  * Use dh --with pgxs_loop and run tests at build-time as well.
  * DH 13.
  * debian/tests: Use installed-versions instead of supported-versions.
  * debian/tests: Run installcheck via autopkgtest as well, next to our custom
    test.

 -- Christoph Berg <myon@debian.org>  Wed, 07 Oct 2020 11:10:17 +0200

pg-cron (1.2.0-1) unstable; urgency=medium

  * New upstream version with PG12 support.
  * Fix testsuite for compatibility with 9.x.

 -- Christoph Berg <myon@debian.org>  Sat, 26 Oct 2019 09:23:54 +0200

pg-cron (1.1.4-1) experimental; urgency=medium

  * New upstream version.

 -- Christoph Berg <myon@debian.org>  Wed, 10 Apr 2019 15:01:26 +0200

pg-cron (1.1.3-2) unstable; urgency=medium

  * Recompile against new heap_getattr() API in PostgreSQL 11.2.

 -- Christoph Berg <christoph.berg@credativ.de>  Tue, 12 Feb 2019 15:07:34 +0100

pg-cron (1.1.3-1) unstable; urgency=medium

  * New upstream version.

 -- Christoph Berg <christoph.berg@credativ.de>  Thu, 29 Nov 2018 09:32:14 +0100

pg-cron (1.1.2-3) unstable; urgency=medium

  * Upload for PostgreSQL 11.
  * Use -Wno-unknown-warning-option instead of removing -Werror.
    (Supported by clang, and -Wno-* is ignored by gcc by default.)

 -- Christoph Berg <myon@debian.org>  Fri, 12 Oct 2018 12:45:13 +0200

pg-cron (1.1.2-2) unstable; urgency=medium

  * Remove -Werror so clang doesn't die on -Wno-maybe-uninitialized with PG11.

 -- Christoph Berg <myon@debian.org>  Sun, 29 Jul 2018 11:57:56 +0200

pg-cron (1.1.2-1) unstable; urgency=medium

  * New upstream version with PG 11 support.
  * Set CC=gcc to enable -Wno-maybe-uninitialized in Makefile.
  * Move packaging repository to salsa.debian.org
  * Move maintainer address to team+postgresql@tracker.debian.org.

 -- Christoph Berg <myon@debian.org>  Sat, 28 Jul 2018 20:39:10 +0200

pg-cron (1.0.2-1) unstable; urgency=medium

  * Initial release.
  * Fix type mismatches on 32bit.
  * Disable warnings about maybe-uninitialized variables, Ubuntu/ppc64el needs
    this for entry.c.

 -- Christoph Berg <christoph.berg@credativ.de>  Mon, 27 Nov 2017 10:31:29 +0100
